"use strict";

const
  _ = require('lodash'),
  conf = require('thebot-conf'),
  redis = conf.redis(),
  redisSubs = conf.redis('subs'),
  log = conf.log('default', { name: 'ircio-worker' }),
  joinQueue = require('thebot-joinqueue')(redis),
  RateLimiter = require('./rate-limiter.js'),
  ConnectionService = require('./connection-service.js'),
  db = require('thebot-db')(),
  Channel = db.Channel,
  ObjectId = db.db.Types.ObjectId;

const ip = _.chain(require('os').networkInterfaces())
  .values()
  .flatten()
  .filter(function(val){ return val.family === 'IPv4' && !val.internal; })
  .pluck('address')
  .first().value();

log.info('Running local IRC rate limiter on IP "%s"', ip);

let rateLimiter = new RateLimiter({
  redis: redis,
  ip: ip
});

/**
 * Configuration for newly created ConnectionServices.
 */
let connConfig = {
  rateLimiter: rateLimiter,
  irc: {
    nick: conf.get('irc.nick'),
    pass: conf.get('irc.pass'),
    proxy: false
  }
};

log.info({ connectionConfig: connConfig }, 'Using this configuration for new connections');

let
  connections = [],
  channelInfo = {},
  channelNameToId = {};

const
  maxChannelsPerConnection = conf.get('ircLimits.maxChannelsPerConnection'),
  maxChannelLoadThreshold = conf.get('maxChannelLoadThreshold');

/**
 * Listens to messages from the parent process.
 */
process.on('message', function(msg){
  switch(msg.op){
  case 'join':
    channelJoinRequest(new ObjectId(msg.channel));
    break;
  case 'purge':
    _.invoke(connections, 'purgeChannels');
    break;
  }
});

/**
 * Handles incoming redis messages for the channels that we handle.
 */
redisSubs.on('message', function(redisChannel, message){
  let
    channelId = new ObjectId(redisChannel.substring(redisChannel.lastIndexOf(':')+1)),
    info = channelInfo[channelId.id];
  
  if(info){
    info.conn.processChannelMesage(info.name, JSON.parse(message));
  } else {
    log.warn({ coreMessage: message }, 'Incoming redis message for channel %s (in subscription "%s") could not be ' +
      'delivered: Unknown channel', channelStr(channelId), redisChannel);
    
    unsubscribeChannel(channelId.id);
  }
});

/**
 * Handles a channel join request from the parent process
 * @param {ObjectId} channelId
 */
function channelJoinRequest(channelId){
  Channel.findInDatabase(channelId, null, Channel.ensureEntity(function(err, dbChannel){
    if(err){
      log.error(err, 'Error while trying to get channel %s from the database, discarding channel',
        channelStr(channelId));
      // If we can't find the channel in the database, it was deleted or it never existed, this is a condition
      // that should never happen unless there is an programmer error in thebot's other layers. So we just
      // discard the channel and hope someone sees the logs.
      discardChannelInParentProcess(channelId);
      return;
    }

    findAppropriateConnection(dbChannel, function(err, conn){
      if(err){
        log.error(err, 'Error while finding an appropriate connection for channel %s, discarding channel',
          channelStr(channelId, dbChannel.twitch.name));
        discardChannelInParentProcess(channelId);
        return;
      }

      if(conn){
        joinChannel(conn, dbChannel);
      } else {
        createNewConnection(dbChannel);
      }
    });
  }));
}

/**
 * Finds a connection that is likely to be able to sustain the given channel load.
 * TODO Implement recent channel load statistics to better determine if an active connection could handle the load
 * 
 * @param dbChannel the channel
 * @param {function} callback
 */
function findAppropriateConnection(dbChannel, callback){
  let
    minLoad = 1.0,
    minLoadConn;
  
  // This is linear but we don't care, we shouldn't have more than few dozen connections
  connections.forEach(function(conn){
    if(conn.load < minLoad && conn.channelCount < maxChannelsPerConnection){
      minLoad = conn.load;
      minLoadConn = conn;
    }
  });
  
  callback(null, minLoad < maxChannelLoadThreshold? minLoadConn : null);
}

/**
 * Creates a new connection to the IRC servers.
 * The creation of the connection is rate-limited. 
 * If the optional channel is specified, it is joined after the connection is established.
 * @param [dbChannel]
 */
function createNewConnection(dbChannel){
  let conn = new ConnectionService(_.uniqueId(), connConfig);

  conn.on('joinChannel', connJoinChannel);
  conn.on('partChannel', connPartChannel);
  conn.on('loadUpdate', connLoadUpdate);
  conn.on('abortChannel', connAbortChannel);
  conn.on('abort', connAbort);

  connections.push(conn);

  conn.log.info('New connection created');
  
  if(dbChannel){
    joinChannel(conn, dbChannel);
  }
}

function joinChannel(conn, dbChannel){
  channelInfo[dbChannel._id.id] = {
    conn: conn,
    name: dbChannel.twitch.name
  };
  channelNameToId[dbChannel.twitch.name] = dbChannel._id;
  subscribeChannel(dbChannel._id);
  conn.joinChannel(dbChannel.twitch.name);
}

function subscribeChannel(channelId){
  redisSubs.subscribe(joinQueue.getChannelMessagesKey(channelId));
}

function unsubscribeChannel(channelId){
  redisSubs.unsubscribe(joinQueue.getChannelMessagesKey(channelId));
}

/**
 * Notifies parent process this channel is no longer in use by this process
 * @param channelId
 */
function discardChannelInParentProcess(channelId){
  process.send({
    op: 'quitChannel',
    channel: channelId
  });
}

/**
 * Quits the channel and closes any resources associated with it. 
 * @param {ObjectId} channelId
 * @param {function} callback
 */
function quitChannel(channelId, callback){
  let info = channelInfo[channelId.id];

  if(info){
    joinQueue.delJoinStatus(channelId.id, function(err){
      if(err){
        log.fatal(err, 'Error while deleting channel %s join status', channelStr(channelId));
        throw err; // necessary?
        //return;
      }
          
      callback(info);

      // Ignore any further command sent to this channel
      delete channelInfo[channelId.id];
      unsubscribeChannel(channelId.id);
    });
  } else{
    log.warn('Trying to quit from unknown channel %s', channelStr(channelId));
  }
}

/**
 * Quits the channel and puts it back to the join queue.
 * @param channelId
 */
function abortChannel(channelId, callback){
  quitChannel(channelId, function(info){
    let priority = _.parseInt('7' + _.now().toString());
    
    joinQueue.enqueueJoin(channelId.id, { priority: priority }, function(err){
      if(err){
        log.fatal(err, 'Unable to enqueue join for channel %s after channel was aborted', channelStr(channelId));
        // We need a better way to handle this than throwing, we might just retry forever, since join queuing
        // is not *that* important, but in reality, errors should never happen here unless its a connection error
        // in which case we need the global node_redis 'offline queue' to persist before actually calling us 
        // with an error. So for now we just throw, in the future we log and return and we handle connection
        // errors in a redis error callback, shutting down there
        throw err;
      }
      
      // Notify parent process this channel is no longer in use by this process
      discardChannelInParentProcess(channelId);

      callback();
    });

    // Send a PART command to the channel
    info.conn.leaveChannel(info.name);
  });
}

/**
 * Sets the channel join status to joined.
 * If an error occurs the channel is aborted.
 * @param channelName
 */
function connJoinChannel(channelName){
  /* jshint validthis: true */
  let
    channelId = channelNameToId[channelName],
    conn = this;

  joinQueue.setJoinStatus(channelId.id, 'joined', function(err){
    if(err){
      conn.log.error(err, 'Error while setting channel %s join status to "joined"', channelStr(channelId));
      abortChannel(channelId);
      return;
    }

    conn.log.info('Channel %s is completely joined', channelStr(channelId));
  });
}

/**
 * Parts from the specified channel and closes any resources associated with it.
 * @param channelName
 */
function connPartChannel(channelName){
  let channelId = channelNameToId[channelName];

  quitChannel(channelId, function(){
    discardChannelInParentProcess(channelId);
  });
}

/**
 * Returns the aborted channel back to the join queue.
 * @param {string} channelName
 */
function connAbortChannel(channelName){
  /* jshint validthis: true */
  let channelId = channelNameToId[channelName];

  this.log.warn('Returning aborted channel %s back to the JOIN queue', channelStr(channelId));

  abortChannel(channelId, function(){
    delete channelNameToId[channelName];
  });
}

function connLoadUpdate(){
  /* jshint validthis: true */
}

/**
 * Returns the aborted connection's channels back to join queue and destroys the connection.
 * @param {string[]} channelNames
 */
function connAbort(channelNames){
  /* jshint validthis: true */
  this.log.warn({ channels: channelNames }, 'Connection aborted. Returning it\'s channels back to the JOIN queue.');

  channelNames.forEach(function(chName){
    abortChannel(channelNameToId[chName], function(){
      delete channelNameToId[chName];
    });
  });

  connections.splice(connections.indexOf(this), 1);
}

function channelStr(channelId, name){
  let
    ret = '"',
    chName = name || (channelInfo[channelId.id] && channelInfo[channelId.id].name);

  if(chName){
    ret = 'chN: ' + chName + ', ';
  }

  ret += 'chId: ' + channelId.toString() + '"';

  return ret;
}