"use strict";

var
  _ = require('lodash');

module.exports = CommandParser;

var globalRegExHelper = _.memoize(function(regString){
  return new RegExp(regString, 'gi');
});

function getGlobalRegEx(regString){
  var rx = globalRegExHelper(regString);
  rx.lastIndex = 0;
  return rx;
}

var getCommandRegEx = _.memoize(function(regString){
  return new RegExp(regString, 'i');
});

// Fix for circular reference jshint
var getRegExTypeInfo;

var regExTypeChunks = {
  'string': '\\S+',
  
  'integer': '-?\\d{1,9}',
  
  'unsigned': '\\d{1,9}',
  
  'float': '-?\\d{0,9}\\.?\\d{1,9}',
  
  'unsigned float': '\\d{0,9}\\.?\\d{1,9}',
  
  'range': '\\d{1,9}\\s?-\\s?\\d{1,9}',
  
  'set': function(args){
    if(!args || args.length === 0){
      return false;
    }
    return args.map(escapeForRegEx).join('|');
  },
  
  'list': function(args){
    if(!args || args.length === 0){
      return false;
    }
    
    let tinfo = getRegExTypeInfo(args[0]);
    return tinfo.chunk? '(?:' + tinfo.chunk + '(?:\\s+|$))+' : false;
  }
};

/**
 * Returns a RegEx chunk for the given type definition.
 */
getRegExTypeInfo = _.memoize(function(typeDef){
  var
    tinfo = parseTypeDef(typeDef),
    chunk = regExTypeChunks[tinfo.type];
  
  if(_.isFunction(chunk)){
    chunk = chunk(tinfo.args);
  }
  
  return _.extend(tinfo, { chunk: chunk });
});

function parseTypeDef(type){
  var typeArgs;
  
  type = type.replace(/\{(.*)\}/, function(match, args){
    typeArgs = args.split(',');
    return '';
  });
  
  return { type: type, args: typeArgs };
}

var typeProcessors = {
  'date': function(input){
    // return as ms since epoch
  },
  
  'integer': _.parseInt,
  
  'unsigned': _.parseInt,
  
  'float': parseFloat,
  
  'unsigned float': parseFloat,
  
  'range': function(input){
    input = input.split('-');
    return {
      start: _.parseInt(input[0]),
      end: _.parseInt(input[1])
    };
  },
  
  'set': function(input, tinfo){
    let originalOption;
    tinfo.args.forEach(function(option){
      if(option.toLowerCase() === input.toLowerCase()){
        originalOption = option;
        return false;
      }
    });
    return originalOption;
  },
  
  'list': function(input, tinfo){
    tinfo = getRegExTypeInfo(tinfo.args);
    let itemTypeRx = getGlobalRegEx(tinfo.chunk + '(?:\\s+|$)');
    
    return input.match(itemTypeRx).map(function(itemInput){
      return processTypeInput(itemInput, tinfo);
    });
  }
};

function processTypeInput(input, tinfo){
  var typeProcessor = typeProcessors[tinfo.type];
  return typeProcessor? typeProcessor(input, tinfo) : input;
}

function escapeForRegEx(s){
  return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

function CommandParser(opts){
  this.maxDepth = opts.maxDepth || 5;
  this.commands = {};
}

CommandParser.prototype.parseRaw = function(input){
  if(input.length > 1 && (input[0] === '#' || input[0] === '!')){
    var ret = this.parseCommandFromIndex(input, (/^.gr8 /i.test(input)? 5 : 1), 0);
    if(ret && (typeof ret.validPrefix === 'undefined' || ret.validPrefix.indexOf(input[0]) !== -1)){
      delete ret.validPrefix;
      return ret;
    }
  }
};

CommandParser.prototype.parseFromId = function(input, depth){
  if(!depth){
    depth = 0;
  }
  
  return depth < this.maxDepth && this.parseCommandFromIndex(input, 0, depth);
};

CommandParser.prototype.parseCommandFromIndex = function(input, index, depth){
  var cmdEnd = input.indexOf(' ', index);
  if(cmdEnd === -1){
    cmdEnd = input.length;
  }
  
  var res = this.findCommand(input.slice(index, cmdEnd), input, cmdEnd);

  if(res){
    return this.tryMatchCommandInput(res.cmd, input.slice(res.end), depth);
  }
  
  return false;
};

CommandParser.prototype.findCommand = function(firstPart, input, index){
  var cmd;

  if(this.commands.hasOwnProperty(firstPart)){
    cmd = this.commands[firstPart];
  } else {
    firstPart = firstPart.toLowerCase();
    cmd = this.commands.hasOwnProperty(firstPart) && this.commands[firstPart];
  }

  if(cmd){
    if(cmd._c){
      var
        nested = cmd._c,
        parts = input.slice(index).split(' '),
        payload = 0;

      parts.forEach(function(part){
        payload += part.length + 1;

        if(part.length > 0){
          if(nested.hasOwnProperty(part)){
            nested = nested[part];
          } else {
            part = part.toLowerCase();
            if(nested.hasOwnProperty(part)){
              nested = nested[part];
            } else {
              return false;
            }
          }

          if(nested._leaf){
            cmd = nested;
            index += payload;
            payload = 0;
          }

          if(nested._c){
            nested = nested._c;
          } else {
            return false;
          }
        }
      });
    }

    return cmd._leaf && { cmd: cmd, end: index };
  }
};

CommandParser.prototype.tryMatchCommandInput = function(cmd, input, depth){
  var cmdArgs = this.parseCommandArgs(cmd, input);

  if(cmdArgs){
    if(cmd.aliasCommand){
      var aliasedCmd = cmd.aliasCommand.replace(/\$(\w+)/g, function(match, argName){
        return cmdArgs[argName] || '';
      });

      return this.parseFromId(aliasedCmd, depth+1);
    } else {
      cmdArgs.id = cmd.id;
      cmdArgs.validPrefix = cmd.validPrefix;
      return cmdArgs;
    }
  }
  
  return false;
};

CommandParser.prototype.parseCommandArgs = function(cmd, input){
  if(cmd.argRegEx){
    var
      matches = cmd.argRegEx.exec(input),
      isRaw = !!cmd.aliasCommand;
    
    if(matches){
      var ret = {};

      for(var i = 0, len = cmd.args.length, k = 1; i < len; ++i){
        var
          arg = cmd.args[i],
          typeInfo = cmd.args._tinfo[i];
            
        // Find the correct type matched
        for(var t = 0, typeLen = arg.types.length; t < typeLen; ++t, ++k){
          if(matches[k]){
            ret[arg.name] = isRaw?  matches[k] : processTypeInput(matches[k], typeInfo[t]);
            break;
          }
        }
        
        // Advanced to the next capture group
        k += typeLen - t;
      }
      
      return ret;
    }
  } else {
    return {};
  }
  
  return false;
};

CommandParser.prototype.addCommand = function(cmd){
  var ok = true;
  
  if(cmd.args && cmd.args.length > 0){
    ok = cmd.argRegEx = this.getArgumentsRegEx(cmd.args);
  }
  
  if(ok){
    cmd._leaf = true;

    this.commands[cmd.id.toLowerCase()] = cmd;

    if(cmd.aliases){
      cmd.aliases.forEach(function(alias){
        alias =  alias.toLowerCase().split(' ');

        var
          cmds = this.commands,
          lastEl = alias.length - 1;

        alias.forEach(function(part, i){
          if(cmds[part]){
            if(i === lastEl){
              throw new Error('command already exist\'s');
            } else {
              if(!cmds[part]._c){
                cmds[part]._c = {};
              }
              cmds = cmds[part]._c;
            }
          } else {
            if(i === lastEl){
              cmds[part] = cmd;
            } else {
              cmds[part] = {
                _c: {}
              };
              cmds = cmds[part]._c;
            }
          }
        });
      }, this);
    }
  }
  
  return !!ok;
};

/**
 * Generate a RegExp from the supplied argument definition array.
 * We cache the generated RegEx object since there is a finite number of definitions.
 */
CommandParser.prototype.getArgumentsRegEx = function(args){
  var
    optionalOn = false,
    regStr = '';
  
  args._tinfo = [];
    
  for(var i = 0, len = args.length; i < len; ++i){
    var arg = args[i];
    
    if(optionalOn){
      // After the first optional argument, every argument following it must be optional
      if(!arg.optional){
        return false;
      }
    } else if(arg.optional){
      optionalOn = true;
    }
    
    args._tinfo[i] = [];
    
    var typeRegs = [];
    for(var t = 0, typeLen = arg.types.length; t < typeLen; ++t){
      var tinfo = getRegExTypeInfo(arg.types[t]);
      
      if(!tinfo.chunk){
        return false;
      }
      
      typeRegs.push('(' + tinfo.chunk + ')');
      args._tinfo[i][t] = tinfo;
    }
    
    if(i === 0){
      regStr += '^\\s*' + (typeRegs.length === 1? typeRegs[0]: '(?:' + typeRegs.join('|') + ')');
    } else {
      regStr += '(?:\\s+' + (typeRegs.length === 1? typeRegs[0]: '(?:' + typeRegs.join('|') + ')') + ')';
    }
    
    if(optionalOn){
      regStr += '?';
    }
  }
  
  return getCommandRegEx(regStr);
};


CommandParser.prototype.delCommand = function(id){
  var cmd = this.commands[id.toLowerCase()];
  
  if(cmd){
    // use cmd.id instead of id, since id may be an alias
    this.delCommandTree(cmd, this.commands, [cmd.id.toLowerCase()], 0, 0);
    
    if(cmd.aliases){
      cmd.aliases.forEach(function(alias){
        alias  = alias.toLowerCase().split(' ');

        this.delCommandTree(cmd, this.commands, alias, 0, alias.length - 1);
      }, this);
    }
    
    return true;
  }
  
  return false;
};

CommandParser.prototype.delCommandTree = function(cmd, cmds, parts, i, lastEl){
  var
    part = parts[i],
    parent = cmds[part];

  if(i === lastEl){
    // Only delete if the alias points to the same command, i.e. it hasn't been overwritten
    if(parent === cmd){
      if(parent._c){
        cmds[part] = { _c: parent._c };
      } else {
        delete cmds[part];
      }
    }
  } else {
    this.delCommandTree(cmd, parent._c, parts[i+1], i+1, lastEl);

    // Remove path to the leaf if no other children
    if(Object.keys(parent._c).length === 0){
      if(!parent._leaf){
        delete cmds[part];
      } else {
        delete parent._c;
      }
    }
  }
};

CommandParser.prototype.purgeCommands = function(){
  this.commands = {};
};