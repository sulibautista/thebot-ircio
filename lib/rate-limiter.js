/** @module rate-limiter **/

"use strict";

const
  _ = require('lodash'),
  conf = require('thebot-conf');

module.exports = RateLimiter;

// For testing
RateLimiter.ConnectionJoinRateLimiter = ConnectionJoinRateLimiter;
RateLimiter.MessageRateLimiter = MessageRateLimiter;

/**
 * Interface for the various rate limiters returned by a {@link RateLimiter}
 * @constructor IRateLimiter
 */

/**
 * Runs then given function as soon as the rate limit allows it. 
 * @name IRateLimiter#run
 * @function
 * @abstract
 * @param {function(err)} callback The function to call
 */

/**
 * This class abstracts rate limiting configuration of IRC servers by providing a single interface that
 * doesn't need to change even if the limits change in structure.
 * @constructor
 */
function RateLimiter(opts){
  this.opts = opts;
  this.connJoinRateLimiter = new ConnectionJoinRateLimiter(opts);
  this.msgLimiters = {};
}

/**
 * Returns an appropriate rate limiter for TCP connections.
 * @returns {IRateLimiter}
 */
RateLimiter.prototype.connection = function(){
  return this.connJoinRateLimiter;
};

/**
 * Returns an appropriate rate limiter for IRC JOIN commands.
 * @param {string|number} connection An unique identifier that describes the connection being used
 * @returns {IRateLimiter}
 */
RateLimiter.prototype.join = function(connection){
  return this.connJoinRateLimiter;
};

/**
 * Returns an appropriate rate limiter for IRC PRIVMSG commands.
 * @param {string|number} connection An unique identifier that describes the connection being used
 * @param {string } [channel] The ID of the channel where the message will be send.
 * @returns {IRateLimiter}
 */
RateLimiter.prototype.message = function(connection, channel){
  let limiter = this.msgLimiters[connection];
  if(!limiter){
    this.msgLimiters[connection] = limiter = new MessageRateLimiter(this.opts);
  }
  
  return limiter;
};

/**
 * Releases all resources attached to a connection used in either join() or message() when it's closed.
 * If the connection wans't used on the mentioned methods, this does nothing.
 */
RateLimiter.prototype.endConnection = function(connection){
  let msgLimiter = this.msgLimiters[connection];
  if(msgLimiter){
    this.msgLimiters[connection].destroy();
    delete this.msgLimiters[connection];
  }
};

/**
 * Releases all resources attached to a channel used in message() when it's PART'd.
 * If the channel wasn't used in message(), this does nothing.
 * @param {string} channel
 */
RateLimiter.prototype.endChannel = function(channel){
  
};

/**
 * As of 04/08/2014 Twitch uses a shared rate limit for both authorizations (connections) and JOINs. 
 * @private
 * @constructor
 * @extends IRateLimiter
 * @param opts
 */
function ConnectionJoinRateLimiter(opts){
  this.redis = opts.redis;
  this.limitsKey = this.redisBaseKey + opts.ip;
  this.queue = [];
  this.queueMode = false;
  this.timeout = null;
}

ConnectionJoinRateLimiter.prototype.limit = conf.get('ircLimits.joinAuth');
ConnectionJoinRateLimiter.prototype.timeframe = conf.get('ircLimits.joinAuthTimeframe');
ConnectionJoinRateLimiter.prototype.redisBaseKey = 'ircio:rate-limit:connectionJoin:';

ConnectionJoinRateLimiter.prototype.run = function(callback){
  if(this.queueMode){
    this.queue.push(callback);
  } else {
    this.tryGetSingleAllowance(callback);
  }
};

/**
 * @private
 */
ConnectionJoinRateLimiter.prototype.tryGetSingleAllowance = function(callback){
  this.tryGetAllowance(1, function(err, res){
    if(err){
      callback(err);
      return;
    }
    
    if(res.allowed === 1){
      callback();
    } else {
      this.queue.push(callback);
      this.setQueueMode(true, res.pttl);
    }
  });
};

/**
 * @private
 */
ConnectionJoinRateLimiter.prototype.setQueueMode = function(on, ttl){
  this.queueMode = on;
  clearTimeout(this.timeout);
  
  if(on){
    this.timeout = setTimeout(this.tryGetQueueAllowance.bind(this), ttl);
  }
};

/**
 * @private
 */
ConnectionJoinRateLimiter.prototype.tryGetQueueAllowance = function(){
  if(this.queue.length > 0){
    this.tryGetAllowance(this.queue.length, function(err, res){
      if(err){
        // call all queue items with an error ??
        throw err;
      }
      
      for(let i = 0; i < res.allowed; ++i){
        this.queue.shift()();
      }
      
      this.setQueueMode(res.available <= 0 || this.queue.length > 0, res.pttl);
    });
  }
};


/* jshint multistr:true */
/**
 * This LUA script maintains a ConnectionJoin rate limit in a given key, it takes care of synchronizing all access to
 * the resource. 
 * By using a volatile KEY we can refresh the rate limit just by recreating the KEY when its deleted.  
 * @private
 */
const getConnectionJoinAllowance =
"local pttl, allowed \
local avail = redis.call('GET', KEYS[1]) \
if avail then \
  allowed = math.min(ARGV[3], avail) \
  if allowed > 0 then \
    redis.call('DECRBY', KEYS[1], allowed) \
    avail = avail - allowed \
  end \
  pttl = math.max(redis.call('PTTL', KEYS[1]), 0) \
else \
  allowed = math.min(ARGV[1], ARGV[3]) \
  avail = ARGV[1] - allowed \
  redis.call('SET', KEYS[1], avail, 'EX', ARGV[2]) \
  pttl = ARGV[2] * 1000 \
end \
return cjson.encode({[\"allowed\"] = allowed, [\"pttl\"] = pttl, [\"available\"] = avail})";
  
/**
 * @private
 */
ConnectionJoinRateLimiter.prototype.tryGetAllowance = function(amount, callback){
  /* jshint evil:true */
  this.redis.eval(
    getConnectionJoinAllowance,
    1, this.limitsKey,
    this.limit,
    this.timeframe,
    amount,
  function(err, res){
    if(err){
      return callback(err);
    }
    try{
      res = JSON.parse(res);
    }catch(e){
      return callback(e);
    }
    callback(null, res);
  });
};

/**
 * As of 04/08/2014 Twitch limits PRIVMSG requests per connection only.
 * @private
 * @constructor
 * @extends IRateLimiter
 * @param opts
 */
function MessageRateLimiter(opts){
  this.currLimit = this.limit;
  this.interval = setInterval(this.processQueue.bind(this), this.timeframe * 1000);
  this.queue = [];
}

MessageRateLimiter.prototype.limit = conf.get('ircLimits.mod.message');
MessageRateLimiter.prototype.timeframe = conf.get('ircLimits.mod.messageTimeframe');
MessageRateLimiter.prototype.messageTtl = conf.get('ircLimits.messageTtl');

MessageRateLimiter.prototype.destroy = function(){
  clearInterval(this.interval);
  delete this.queue; // maybe call all queue items with an error?
};

MessageRateLimiter.prototype.run = function(callback){
  if(this.currLimit > 0){
    --this.currLimit;
    callback();
  } else {
    this.queue.push({
      timestamp: _.now(),
      callback: callback
    });
  }
};

/**
 *  @private
 */
MessageRateLimiter.prototype.processQueue = function(){
  this.currLimit = this.limit;
  
  if(this.queue.length > 0){
    let ttl = _.now() - this.messageTtl;
    
    while(this.queue.length > 0){
      let op = this.queue.shift();
      if(op.timestamp > ttl){
        op.callback();
        
        if(--this.currLimit < 1){
          break;
        }
      }
    }
  }
};