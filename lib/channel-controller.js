"use strict";

const
  _ = require('lodash'),
  util = require('util'),
  EventEmitter = require('events').EventEmitter,

  DefensIRC = require('thebot-defensirc'),
  CommandParser = require('./command-parser.js');

module.exports = ChannelController;

/**
 *
 * @param opts
 */
function ChannelController(opts){
  EventEmitter.call(this);
  
  this.connId = opts.connId;
  this.channelName = opts.channelName;
  this.rateLimiter = opts.rateLimiter;
  this.botCore = opts.botCore;
  this.ircClient = opts.ircClient;
  this.log = opts.log.child({
    component: 'ChannelController',
    channel: this.channelName
  });
  this.joined = false;
  this.commandParser = new CommandParser({});

  this.modUpdateTime = 5 * 60 * 1000;
  this.modList = [];
  this.modTimer = null;

  // TODO Once we know how to detect if the core connection is openned / closed, we can do this only if we are connected
  this.onCoreConnect();
}
util.inherits(ChannelController, EventEmitter);

/**
 * Tries to JOIN to the channel whiting the join rate limits. 
 */
ChannelController.prototype.join = function(){
  let self = this;
    
  this.rateLimiter.join(this.connId).run(function(err){
    if(err){
      // might actually try to retry, but rate limiter errors are most likely to persist so we just abort
      self.log.error(err, 'Error requesting JOIN allowance from the RateLimiter, aborting channel');
      self.emit('abort');
      return;
    }
    
    self.ircClient.join(self.channelName, function(err){
      /*if(err){
        if(err instanceof DefensIRC.NotConnectedError){
          // at this point the connection service should be reconnecting or terminating, so we let it do its job
          logger.log('warn', 'Channel "%s" could not be JOINed because the IRC connection is closed', self.channelName);
        } else {
          // any other errors shouldn't actually be happening, if so discard the channel
          logger.log('error', 'Error while requesting IRC JOIN for channel "%s", aborting channel',
              self.channelName, err);
          self.emit('abort');
        }
        return;
      }*/

      self.joined = true;
      self.emit('join');
      self.checkMods();
    });
  });
};

/**
 * Tries to PART from the channel.
 */
ChannelController.prototype.part = function(){
  if(this.joined){
    let self = this;
    this.ircClient.part(this.channelName, function(err){
      /*if(err){
       self.log.warn(err, 'Error while PARTing from channel');
       return;
       }*/

      self.joined = false;
      clearTimeout(self.modTimer);
      self.emit('part');
    });
  }
};

ChannelController.prototype.scheduleModTimer = function(){
  clearTimeout(this.modTimer);

  let self = this;
  this.modTimer = setTimeout(function(){
    self.modList = [];
    self.checkMods();
  }, this.modUpdateTime);

  this.modTimer.unref();
};

ChannelController.prototype.checkMods = function(){
  this.scheduleModTimer();
  this.write('.mods');
};

ChannelController.prototype.handleIRCOperator = _.debounce(function(op, user){
  this.checkMods();
}, 5000);

ChannelController.prototype.handleIRCMessage = function(from, msg){
  this.log.trace('IRC Message from: %s msg: %s', from, msg);

  if(from === 'jtv'){
    this.parseTwitchMessage(msg);
  } else {
    let cmd = this.commandParser.parseRaw(msg);
    if(cmd){
      console.log(this.channelName, from, cmd);
      this.sendUpstream(cmd, from);
    }
  }
};

ChannelController.prototype.parseTwitchMessage = function(msg){
  let mods = /^The moderators of this room are:(.*)/.exec(msg);
  if(mods){
    mods = mods[1].split(',').map(function(mod){ return mod.trim(); });
    if(mods.length !== this.modList.length || _.intersection(mods, this.modList).length !== mods.length){
      this.modList = mods;
      this.sendUpstream({ id: 'updatemods', mods: mods });
    }
  } else if(!/USERCOLOR|HISTORYEND|EMOTESET|SPECIALUSER/.test(msg)){
    this.log.info('Ignored unknown Twitch protocol message: "%s"', msg);
  }
};

/**
 * Handles a message from thebot-core.
 * We catch some exceptions that may arise from the parsing of the commands.
 */
ChannelController.prototype.handleCoreMessage = function(msg){
  try{
    switch(msg.type){
    case 'write':
      this.write(msg.output);
      break;
    case 'addCommands':
      _.forEach(msg.commands, this.commandParser.addCommand, this.commandParser);
      break;
    case 'delCommands':
      _.forEach(msg.commands, this.commandParser.delCommand, this.commandParser);
      break;
    case 'allCommands':
      this.commandParser.purgeCommands();
      _.forEach(msg.commands, this.commandParser.addCommand, this.commandParser);
      break;
    case 'part':
      this.part();
      break;
    default:
      throw new Error('not implemented');
    }
  } catch(err){
    this.log.error(err, 'Exception thrown while processing core message of type "%s"', msg.type);
  }
};

ChannelController.prototype.onCoreConnect = function(){
  this.sendUpstream({ id: 'getallcommands' });
};

/**
 * Sends a command message to the botCore.
 * TODO implement botCore offline queue and reconnect resync of commands
 */
ChannelController.prototype.sendUpstream = function(cmd, user){
  cmd.userName = user || this.channelName;
  cmd.channelName = this.channelName;
  cmd.channelService = 'twitch';
  this.botCore.emit('command', cmd);
};

ChannelController.prototype.write = function(msg){
  let self = this;
  this.rateLimiter.message(this.connId, this.channelName).run(function(err){
    if(err){
      self.log.error(err, 'Error requesting MESSAGE allowance from the RateLimiter');
      return;
    }

    self.ircClient.say(self.channelName, msg);
  });
};