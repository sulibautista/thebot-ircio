"use strict";

const
  _ = require('lodash'),
  util = require('util'),
  EventEmitter = require('events').EventEmitter,
  sioClient = require('socket.io-client'),
  
  conf = require('thebot-conf'),
  log = conf.log(),
  DefensIRC = require('thebot-defensirc'),
  
  ChannelController = require('./channel-controller.js');

module.exports = ConnectionService;

const
  initLoadPerChannel = conf.get('initialLoadPerChannel');

/**
 * Manages a set of IRC channels in a single IRC connection.
 * An IRC connection and a thebot-core connection are maintained per ConnectionService. All reconnection logic
 * for those connections is handled here, if at some point any of the connection is past the reconnection limits,
 * the service is terminated.
 * 
 * Channel performance statistics are aggregated and emitted every few seconds. 
 * The service also routes messages from the outside to their respective channels.
 * 
 * TODO find a way to now allow botCore.sendBuffer array to grow forever, maybe dont buffer at all? or just ltrim 
 * to the last X messages, we will end up implementing our own circular buffer, or not? we should keep msgs based
 * on time not max count.
 * 
 * TODO Rotate twitch ports 80, 6667 and 447
 * 
 * @constructor
 * @param {string|number} id An ID for this connection, used for logging
 * @param {object} opts
 */
function ConnectionService(id, opts){
  EventEmitter.call(this);
  
  this.connId = id;
  this.rateLimiter = opts.rateLimiter;
  this.channelControllers = {};
  this.sentMessages = 0;
  this.ircConnectionAttemps = 0;
  this.statsInterval = null;
  this.channelCount = 0;
  this.load = 0;
  
  // we use one thebot-core connection per connection service
  this.botCore = sioClient(conf.get('thebot-core.url'), {
    reconnection: true,
    reconnectionAttempts: Infinity,
    reconnectionDelay: 1000,
    reconnectionDelayMax: 30000,
    timeout: 20000,
    autoConnect: false
  });

  this.log = log.child({
    component: 'ConnectionService',
    connectionId: id
  });
    
  this.botCore
    .on('connect', this.onCoreConnect.bind(this))
    .on('connect_timeout', this.onCoreConnectTimeout.bind(this))
    .on('disconnect', this.onCoreDisconnect.bind(this))
    .on('reconnect_failed', this.onCoreReconnectFailed.bind(this));
  
  // we use one IRC client per connection service
  this.ircClient = new DefensIRC(opts.irc);
  
  this.ircClient
    .on('connect', this.onIRCConnect.bind(this))
    .on('message', this.onIRCMessage.bind(this))
    .on('+mode', this.onIRCMode.bind(this))
    .on('-mode', this.onIRCMode.bind(this))
    .on('send', this.onIRCSend.bind(this))
    .on('error', this.onIRCError.bind(this))
    .on('close', this.onIRCClose.bind(this))
    .on('abort', this.onIRCClose.bind(this))
    .on('protocolError', this.onIRCProtocolError.bind(this));
  
  this.startService();
}
util.inherits(ConnectionService, EventEmitter);

ConnectionService.prototype.maxIRCReconnectionAttemps = conf.get('ircLimits.reconnectionAttemps');

/**
 * Starts a previously stopped service.
 */
ConnectionService.prototype.startService = function(){
  this.stopped = false;
  this.botCore.open();
  this.connectIRC();
  
  let self = this;
  this.statsInterval = setInterval(function(){
    self.doStats();
    self.sentMessages = 0;
  }, this.rateLimiter.message(this.connId).timeframe * 1000);
};

/**
 * Update message throughput statistics.
 */
ConnectionService.prototype.doStats = function(){
  this.load = this.sentMessages / this.rateLimiter.message(this.connId).limit;
  this.emit('loadUpdate');
};

/**
 * Joins the specified channel
 * @param {string} channel
 */
ConnectionService.prototype.joinChannel = function(channel){
  let controller = this.channelControllers[channel] = new ChannelController({
    connId: this.connId,
    channelName: channel,
    rateLimiter: this.rateLimiter,
    botCore: this.botCore,
    ircClient: this.ircClient,
    log: log
  });
  
  let self = this;
  
  controller
    .on('abort', function(){
      self.abortChannel(this.channelName);
    })
    
    .on('join', function(){
      self.emit('joinChannel', this.channelName);
    })

    .once('part', function(){
      self.leaveChannel(this.channelName);
    });
    
  if(this.ircClient.readyState === 'open'){
    controller.join();
  }

  ++this.channelCount;
  this.load += initLoadPerChannel;

  this.log.info('Joining channel "%s"', channel);
};

/**
 * Tries connection to the IRC server if it makes sense.
 * @private
 */
ConnectionService.prototype.connectIRC = function(){
  if(this.IRCConnectionMakesSense()){
    this.doConnectIRC();
  }
};

/**
 * Returns whether it makes sense to try connecting to the IRC server.
 * @private
 */
ConnectionService.prototype.IRCConnectionMakesSense = function(){
  return !this.stopped && this.ircClient.readyState === 'closed';
};

/**
 * Tries connecting to the IRC server whiting the connection IRC rate limits.
 * {@link ConnectionService@reconnectIRCOrDie} is called if connection fails.
 * @private
 */
ConnectionService.prototype.doConnectIRC = function(){
  let self = this;
  
  this.rateLimiter.connection().run(function(err){
    if(err){
      self.log.error(err, 'Error while requesting IRC connection allowance through the RateLimiter');
      self.reconnectIRCOrDie();
      return;
    }
    
    // is cheap to check this again, so we do that
    if(!self.stopped){
      self.ircClient.connect();
    }
  });
};

/**
 * Tries to reconnect to the IRC server if not already past max IRC connection attempts, if we hit the limit
 * {@link ConnectionService#abort} is called.
 * @private
 */
ConnectionService.prototype.reconnectIRCOrDie = function(){
  if(this.IRCConnectionMakesSense()){
    if(this.ircConnectionAttemps++ < this.maxIRCReconnectionAttemps){
      this.log.info('Reconnecting to IRC, attempt %d of %d', this.ircConnectionAttemps,
          this.maxIRCReconnectionAttemps);
      
      this.doConnectIRC();
    } else {
      this.abortService();
    }
  }
};

/**
 * Routes the given message to a joined channel to be processed.
 * @param {string} channel
 * @param {object} msg
 */
ConnectionService.prototype.processChannelMesage = function(channel, msg){
  let controller = this.channelControllers[channel];
  if(controller){
    controller.handleCoreMessage(msg);
  } else {
    this.log.warn({ coreMessage: msg },
      'Discarding message sent via ConnectionService.processMesage to unknown channel "%s"', channel, msg);
  }
};

/**
 * Leaves the specified channel.
 * @param {string} channel
 */
ConnectionService.prototype.leaveChannel = function(channel){
  let controller = this.channelControllers[channel];
  if(controller){
    controller.part();
    delete this.channelControllers[channel];
    this.emit('partChannel', channel);
  } else {
    this.log.warn('Discarding leave channel request for unknown channel "%s"', channel);
  }
};

/**
 * Leaves all joined channels.
 */
ConnectionService.prototype.purgeChannels = function(){
  _.forOwn(this.channelControllers, function(controller){
    controller.part();
  });
  
  this.channelControllers = {};
};

/**
 * Closes the service gracefully. 
 * All external connections are closed.
 */
ConnectionService.prototype.stopService = function(){
  this.closeService();
  this.emit('close');
};

/**
 * Removes the channel controller from the service and emits an 'abortChannel' event, passing the aborted channel.  
 * @private
 */
ConnectionService.prototype.abortChannel = function(channel){
  if(this.channelControllers[channel]){
    delete this.channelControllers[channel];
    --this.channelCount;
    this.emit('abortChannel', channel);
  } else {
    this.log.warn('Discarding abort channel request for unknown channel "%s"', channel);
  }
};

/**
 * Terminates the service abruptly due to an unrecoverable error (mostly connection errors).
 * The service is closed and 'abort' is emitted with the list of channels the service handled.
 * @private
 */
ConnectionService.prototype.abortService = function(){
  if(!this.aborted){
    let channels = [];
    _.forOwn(this.channelControllers, function(controller){
      channels.push(controller.channelName);
    });
    this.closeService();
    this.emit('abort', channels);
    this.rateLimiter.endConnection(this.connId);
    this.aborted = true;
  }
};

/**
 * All external connections are closed.
 * @private
 */
ConnectionService.prototype.closeService = function(){
  this.stopped = true;
  this.channelControllers = {}; // TODO STOP all channels so they know they can't go on
  this.channelCount = 0;
  this.botCore.close();
  this.ircClient.close();
  clearInterval(this.statsInterval);
};

ConnectionService.prototype.onCoreConnect = function(){
  this.log.info('Core connection open');
  _.invoke(this.channelControllers, 'onCoreConnect');
  //this.abortService();
};

ConnectionService.prototype.onCoreConnectTimeout = function(){
  this.log.warn('Core connection timed out');
  //this.abortService();
};

ConnectionService.prototype.onCoreDisconnect = function(why){
  this.log.info('Core connection closed. Reason: "%s", reconnecting', why);
  this.botCore.io.reconnect();
};

/**
 * If a core connection couldn't be established after configured retries, we abort the service.
 * @private
 */
ConnectionService.prototype.onCoreReconnectFailed = function(){
  this.log.error('Core reconnect failed');
  this.abortService();
};

/**
 * After a successful IRC connection (or reconnection), we join all known channels.
 * If this is the first connection established, all the awaiting channels will be new channels.
 * If we were reconnecting, all channels will be disconnected channels or new channels added while the connection
 * was down.
 * @private
 */
ConnectionService.prototype.onIRCConnect = function(){
  this.log.info('IRC Connection established');
  
  this.ircConnectionAttemps = 0;
  
  _.invoke(this.channelControllers, 'join');
};

/**
 * Routes IRC messages to the appropriate channel.
 * @private
 */
ConnectionService.prototype.onIRCMessage = function(from, to, msg){
  let controller = this.channelControllers[to];
  if(controller){
    controller.handleIRCMessage(from, msg);
  } else {
    this.log.info('Received ignored IRC message from "%s" to "%s", message: "%s"', from, to, msg);
  }
};

ConnectionService.prototype.onIRCMode = function(a,b,c,d, args){
  args = args.args;

  let controller = this.channelControllers[args[0]];
  if(controller){
    controller.handleIRCOperator(args[1][0], args[2]);
  } else {
    this.log.info('Received ignored IRC mode for unknown channel "%s"', args[0]);
  }
};

ConnectionService.prototype.onIRCSend = function(cmd){
  if(cmd === 'PRIVMSG'){
    ++this.sentMessages;
  }
};

/**
 * Logs IRC errors, reconnecting happens after 'close' is fired by the IRC client.
 * TODO  We should try to look for other kind of errors and ignore those (like parse errors).
 * @private
 */
ConnectionService.prototype.onIRCError = function(err){
  this.log.error(err, 'IRC Connection error');
};

/**
 * Try to reconnection when the IRC connection is closed.
 * @private
 */
ConnectionService.prototype.onIRCClose = function(){
  this.log.info('IRC Connection closed');
  this.reconnectIRCOrDie();
};

/**
 * Handles IRC protocol errors, currently we just log and ignore these.
 * @private
 */
ConnectionService.prototype.onIRCProtocolError = function(err){
  this.log.warn(err, 'IRC protocol error');
};