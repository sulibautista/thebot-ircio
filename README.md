

# thebot-irc

## TODO
* Tests
* IRC Reconnection doesnt work because DefensIRC client's readyState property is not actually updated, (is actually
    implemented as a hack) (Fixed?)

## Architecture
An instance of the IRC bot controls many connections to the twitch chat servers, each connection having a 
short amount of channels that they manage. Since a connection don't use much shared state between other
connections, we can safely run many processes handling varying amount of connections

The shared stuff:
	* Join/Auth limits, we handle this in redis from the beginning, since it allows us to recover from 
		crashes without breaking twitch limits

In each channel, the bot listens to both a dynamically set of command triggers (essentially the commands names)
that map to an actual thebot-core command. They also receive data from thebot-core to send as PRIVMSGs to
the chat servers as well as bookkeeping commands.

User command requests are routed thought socket.io to an instance of thebot-core, this connection is persistent
once established, but can be re-routed if the connection is lost. 

Chat write request are sent from thebot-core via a redis pub/sub key, which is individual per channel. In
this case, many thebot-core instances can send out chat messages which is a requirement of the multi-user 
model we are following for the dashboard. Many other messages, primarily for signaling or syncing purposes
are also sent via the same redis key.

Some channels can out-grow the available throughput in one connection, because it is shared between other
channels, in this case we need to ensure a way of safely moving the channel to another connection
(or create a new one) if we hit the mentioned wall. For maximum future-proofness we issue a join command
directly into redis, which will be handled by whatever setup we happen to use at any moment.

Sometimes twitch might disconnect us randomly, in this case we should try to reconnect a few times (bounded
by the auth rate limit) while we maintain the state payload of the channels intact. If the reconnect is not
possible, we issue a rejoin and discard channel data.
 
So about scaling, we don't need to care about any more instances but one, because we are not likely to hit
the limits for a while, and it looks like it might be necessary to run things from only one IP, at least for
the time being. In any case, should we require scaling, its easily accomplished with the network setup
we already choose and it won't require any major changes to the current implementations.

**Index**. Bootstrapper.

	* Manages top-level bookkeeping within redis, this include keep-alive, performance information, etc

**ServiceContainer**. Manages a set of connections, its status and health.

	* Routes JOIN requests to a connection service.	
	
**RateLimiter**. Manages all IRC rate limits.

	* The join/auth rate limit is implemented by a single numeric redis key per IP	

**DefensIRC**. The actual IRC client implementation of the bot.

	* Supports HTTP tunneling, socks and socket connections.

**ConnectionService**. Handles a single IRC connection to the twitch servers.

	* Handles reconnecting policies for the IRC client
	* Maintains a connection to a thebot-core instance
	* Contains various ChannelControllers which take care of the individual messages received per channel
		by the IRC client.
	* Processes PART messages received, and any other non-channel related message which are not IRC internal.
	* Maintains performance statistics between all channels.
	
**ChannelController**. Takes care of all things related to a single channel.

	* Command parsing and messaging between a thebot-core connection
	* Synchronization between command names, and any state payload
	* Basically all the heavy channel business logic goes here.
	
**CommandParser**. Given an array of command descriptors, parses a string of text to its real command name.


## JOINs
For the single instance, non-scalable mode we will fetch the JOIN request directly from redis, and route
them internally to an appropriate ServiceContainer.

The whole instance is a 'Channel handler' in thebot-joinqueue, so all JOINs are attached to this instance.
If the whole instance goes down, the thebot-healthcare will pick it up and rejoin all channels. If an 
individual process from within the instance crashes, we rejoin all channels attached to that instance. So 
we have to maintain a list of what channels belong to what process in the main process. 

## Commands
Each channel manages a different set of command triggers.
Each command configuration is stored on the DB, in the command files or temporarly in memory.
Each command can have many triggers
Commands start with ! or #, NO exceptions

Examples
	!<trigger> <arg1> <arg2>
	#<memory_trigger> <arg1> <arg2>
	
	!|# name1|name2|name3
	


