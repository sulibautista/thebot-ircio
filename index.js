"use strict";

const
  _ = require('lodash'),
  async = require('async'),
  cp = require('child_process'),
  cpuCount = 1 || require('os').cpus().length,
  
  conf = require('thebot-conf'),
  redis = conf.redis(),
  redisSub = conf.redis('joinSub'),
  log = conf.log('default', { name: 'ircio-main' }),
  joinQueue = require('thebot-joinqueue')(redis),
  ObjectId = require('thebot-db/')().db.Types.ObjectId;

let
  services = [],
  serviceIt = -1,
  channelToService = {},
  keepAliveInterval,
  handlingJoins,
  handlerId;

for(let i = 0; i < cpuCount; ++i){
  let svc = cp.fork(__dirname + '/lib/service-container.js', [i]);
  services.push(svc);
  
  installServiceListeners(svc);
}

log.info('Starting ircio service with %d workers', services.length);

// Register ourselves into redis, then listen for joins
requestHandlerKey(function(){
  redisSub.on('message', function(/*channel, message*/){
    if(!handlingJoins){
      return;
    }

    tryDequeueJoin();
  });

  subscribeToJoinNotifications();

  handlingJoins = false;

  // HACK remove this once we implement the irc health utility, we can do this because we are the only entity
  // joining channels, and if we just started all previous channels are effectively in NOT JOINED state
  redis.hgetall(joinQueue.getJoinStatusKey(), function(err, channels){
    if(err){
      throw err;
    }

    if(!channels){
      handlingJoins  = true;
      tryDequeueJoin();
      return;
    }

    async.each(Object.keys(channels), function(id, cb){

      joinQueue.enqueueJoin(id, 0, function(err){
        if(err){
          log.fatal(err, 'Error joining channel recovered channel %s', id);
        } else {
          log.info('Enqueued recovered channel %s', new ObjectId(id));
        }
        cb(err);
      });
    }, function(err){
      if(err){
        throw err;
      }

      redis.del(joinQueue.getJoinStatusKey(), function(err){
        if(err){
          throw err;
        }

        handlingJoins  = true;
        tryDequeueJoin();
      });
    });
  });
});

// FIXME This is not correct by any means, but for now this prevents child processes from not exiting when we crash
process.on('uncaughtException', function(err){
  log.fatal(err, 'Uncaugth exception');
  setTimeout(function(){
    process.exit(1);
  }, 1000);
});

process.on('exit', function(){
  console.log('ircio service terminated');
  _.invoke(services, 'kill');
});

function installServiceListeners(svc){
  svc.on('error', function(err){
    log.error(err, 'Service container error');
  });
  
  svc.on('exit', function(code, signal){
    if(code){
      log.info('Service container exited with code "%d" (with parent signal "%s")', code, signal || 'none');
    } else {
      log.warn('Service container exited abnormally (with parent signal "%s")', signal || 'none');
    }
    process.exit(0);
  });
  
  svc.on('message', function(msg){
    log.info({ message: msg }, 'Message received from service container');
  });
}

function tryDequeueJoin(){
  joinQueue.dequeueJoin(handlerId, function(err, joiningChannel, priority){
    if(!handlingJoins){
      return;
    }
    
    if(err){
      log.error(err, 'Error while calling joinQueue.dequeueJoin(%s)', handlerId);
      if(err.message === 'BOT_MISSING_HANDLER'){
        handleKeepAliveError();
      }
      return;
    }
    
    if(joiningChannel){
      let channelOId = new ObjectId(joiningChannel);

      log.info('Obtained channel "%s" with priority "%s" from joinQueue', channelOId, priority);

      joinChannel(channelOId);
      
      // Keep dequeuing until queue is empty
      tryDequeueJoin();
    }
  });
}

function joinChannel(channelId){
  // Round robin between all available handlers
  serviceIt = (serviceIt+1) % services.length;
  services[serviceIt].send({
    op: 'join',
    channel: channelId
  });

  channelToService[channelId.id] = serviceIt;
}

function requestHandlerKey(callback){
  joinQueue.registerHandler(function(err, hid){
    if(err){
      throw err;
    }
    
    handlerId = hid;
    /*keepAliveInterval = setInterval(function(){
      joinQueue.keepHandlerAlive(handlerId, function(err){
        if(err){
          logger.error('Error while calling joinQueue.keepHandlerAlive(%s)', handlerId, err);
          
          if(err.message() === 'BOT_MISSING_HANDLER'){
            handleKeepAliveError();
          }
        }
      });
    }, 2000);*/
    
    callback();
  });
}

function subscribeToJoinNotifications(){
  redisSub.subscribe(joinQueue.getJoinNotificationsKey(), function(/*err*/){
    // TODO remove this, add global redis error handler then quit
  });
  handlingJoins = true;
}

function unsubscribeToJoinNotifications(){
  redisSub.unsubscribe(joinQueue.getJoinNotificationsKey());
  handlingJoins = false;
}

/**
 * This happens if for some reason we miss a keep alive, the health system thinks we crashed and its taking care
 * of the channels we were joined, so we just "restart" on cold, but we maintain the existing connections, 
 * this way hopefully we rejoin to all the channels ASAP.
 * 
 * Drop all channels while maintaining existing connections.
 * Request a new handler key and go back to work.
 */
function handleKeepAliveError(){
  log.error('Keep Alive miss, all channels dropped.');
  process.exit(1); // TODO remove this once we fix keep alive recovery
  clearInterval(keepAliveInterval);
  _.invoke(services, 'send', {op: 'purge'});
  
  unsubscribeToJoinNotifications();
  
  requestHandlerKey(function(){
    subscribeToJoinNotifications();
  });
}